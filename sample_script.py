__author__ = 'peter'

import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'online_noun.settings')

import django

django.setup()
from noun.models import School, Course, Department, Resource


def populate():
	school_of_tech = add_schools('School of Science and Technology', 'Sci and Tech')
	school_of_health = add_schools('School of Health Sciences', 'Health and Body')
	school_of_agric = add_schools('School of Agricultural Sciences', 'Farming is our pride')
	school_of_mgt = add_schools('School of Management Sciences ', 'Management of the Econmy')

	dept_of_maths = add_dept(
		department_name='Mathematics',
		department_description='Mathemitcal life style',
		department_school=school_of_tech
	)

	add_course(
		course_code='GST-101',
		course_name='Use of English and Communication Skills I',
		course_department=dept_of_maths
	)

	add_course(
		course_code='PHY-111',
		course_name='Elementary Mechanics ',
		course_department=dept_of_maths
	)

	add_course(
		course_code='CIT-212',
		course_name='System Analysis and Design',
		course_department=dept_of_maths
	)

	add_course(
		course_code='MTH-382',
		course_name='Mathematical Methods IV',
		course_department=dept_of_maths
	)

	dept_of_compt = add_dept(
		department_name='Computer Science',
		department_description='Computerizing the world',
		department_school=school_of_tech
	)

	add_course(
		course_code='CIT 101',
		course_name='Computers in Society ',
		course_department=dept_of_compt
	)

	add_course(
		course_code='CIT 143',
		course_name='Introduction to Data Organisation and Management ',
		course_department=dept_of_compt
	)
	add_course(
		course_code='CIT 215',
		course_name='Introduction to Programming Languages',
		course_department=dept_of_compt
	)

	add_course(
		course_code='MTH 241',
		course_name='Introduction to Real Analysis',
		course_department=dept_of_compt
	)


	# Department of info technology
	dept_of_info_tect = add_dept(
		department_name='Information Technology',
		department_description='Computerizing the world',
		department_school=school_of_tech
	)

	dept_of_info_mobile = add_dept(
		department_name='Mobile (Wireless) Communication Technology',
		department_description='Computerizing the world',
		department_school=school_of_tech
	)

	add_course(
		course_code='CIT 741',
		course_name='Information Technology and Software Development',
		course_department=dept_of_info_mobile
	)

	add_course(
		course_code='CIT 758',
		course_name='Wireless Communication II',
		course_department=dept_of_info_mobile
	)

	add_course(
		course_code='CIT 759',
		course_name='Micro Computing and www',
		course_department=dept_of_info_mobile
	)


def add_schools(school_name, school_description):
	print "creating course......"
	school = School.objects.get_or_create(school_name=school_name, school_description=school_description)[0]
	return school


def add_dept(department_name, department_description, department_school):
	print "adding department ........"
	department = Department.objects.get_or_create(
		department_name=department_name,
		department_description=department_description,
		dept_faculty=department_school
	)[0]
	department.save()
	return department


def add_course(course_code, course_name,  course_department):
	print "creating course......"
	course = Course.objects.get_or_create(
		course_code=course_code,
		course_name=course_name,
		course_dept=course_department
	)[0]
	course.save()


if __name__ == '__main__':
	print "Creating a sample database script"
	populate()
