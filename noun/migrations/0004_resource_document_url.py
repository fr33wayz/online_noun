# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noun', '0003_school_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='resource',
            name='document_url',
            field=models.ImageField(upload_to=b'resources', blank=True),
        ),
    ]
