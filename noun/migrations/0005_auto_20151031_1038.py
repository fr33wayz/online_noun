# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noun', '0004_resource_document_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resource',
            name='document_url',
            field=models.FileField(upload_to=b'resources', blank=True),
        ),
    ]
