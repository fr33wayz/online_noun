__author__ = 'peter'
from django.contrib.auth.models import User
from noun.models import UserProfile

from django import forms


class UserForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput())

	class Meta:
		model = User
		fields = ('email', 'password', 'first_name', 'last_name')
		widgets = {
			'email': forms.EmailInput(attrs={'class': 'validate', 'id': 'email'}),
			'password': forms.PasswordInput(attrs={'class': 'validate', 'id': 'password'}),
			'first_name': forms.TextInput(attrs={'class': 'validate', 'id': 'first_name'}),
			'last_name': forms.TextInput(attrs={'class': 'validate', 'id': 'last_name'})
		}


class StudentForm(forms.ModelForm):
	class Meta:
		model = UserProfile
		fields = ('student_id', 'department')
		widgets = {
			'student_id': forms.TextInput(attrs={'class': 'validate', 'id': 'last_name'}),
			'department': forms.Select(attrs={'class': 'browser-default'}),
		}
