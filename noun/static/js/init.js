(function ($) {
    $(function () {
        $('.button-collapse').sideNav();
        $('select').material_select();

        //get all schools department when a school is selected
        $('#schools').click(function () {
            var school_slug;
            school_slug = $(this).attr('href');
            console.log("Clicked a school with slug of" + school_slug);
            $.ajax(
                {
                    url: '/noun/department/',
                    method: 'GET',
                    data: {school_slug: school_slug},
                    success : function (data) {
                        console.log(data)
                    }
                }
            );
            console.log("done.....")
            //$.get('/noun/departments/', {school_slug:school_slug}, function (department) {
            //    console.log("Department Value"+department)
            //})

        })

    }); // end of document ready
})(jQuery); // end of jQuery name space

//$(document).ready(function () {
//    $('.button-collapse').sideNav();
//    $('select').material_select();
//
//    //get all schools department when a school is selected
//    $('schools').click(function () {
//        var school_slug;
//        school_slug = $(this).attr('href');
//        $.ajax(
//            {
//                url: '/noun/departments/',
//                method: 'GET',
//                data: {school_slug: school_slug},
//                context: document.body
//            }
//        ).done(function (department) {
//                console.log("Department " + department)
//            })
//
//    })
//});