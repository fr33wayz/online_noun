from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect

# Create your views here.
from noun.forms import StudentForm, UserForm
from noun.models import Department, School, Resource, Course


@login_required
def index(request):
	context_dict = {}
	return render(request, template_name='noun/_home-page.html', context=context_dict)


def registration(request):
	registered = False

	if request.method == 'POST':
		student_form = StudentForm(data=request.POST)
		user_form = UserForm(data=request.POST)
		if student_form.is_valid() and user_form.is_valid():
			user = user_form.save(commit=False)
			student_profile = student_form.save(commit=False)
			user.username = student_profile.student_id
			user_form.save()
			user.set_password(user.password)
			user.save()
			student_profile.user = user
			# student_profile.department = department
			student_profile.save()
			registered = True
			return redirect('/noun/')
		else:
			print user_form.errors, student_form.errors
	else:
		student_form = StudentForm()
		user_form = UserForm()

	context_dict = {'user_form': user_form, 'student_form': student_form, 'isRegistered': registered}
	return render(request, template_name='noun/_registration_page.html', context=context_dict)


@login_required
def schools_page(request):
	context_dict = {}
	schools = School.objects.all()
	context_dict['schools'] = schools
	return render(request, template_name='noun/_schools_list.html', context=context_dict)


@login_required
def get_schools_department(request, school_slug):
	context_dict = {}
	try:
		school_to_get = School.objects.get(slug=school_slug)
		if school_to_get:
			context_dict['school'] = school_to_get
		departments = Department.objects.filter(dept_faculty=school_to_get)
		if departments:
			context_dict['departments'] = departments
	except School.DoesNotExist, Department.DoesNotExist:
		pass
	return render(request, template_name='noun/_schools_department.html', context=context_dict)


@login_required
def get_department_course(request, dept_id):
	context_dict = {}
	try:
		dept = Department.objects.get(id=dept_id)
		if dept:
			context_dict['dept'] = dept
		courses = Course.objects.filter(course_dept=dept)
		if courses:
			context_dict['courses'] = courses
	except Course.DoesNotExist, Department.DoesNotExist:
		pass
	return render(request, template_name='noun/_course_list.html', context=context_dict)


@login_required
def resources_list(request, course_slug):
	context_dict = {}
	try:
		course = Course.objects.get(slug=course_slug)
		if course:
			context_dict['course'] = course
		resources = Resource.objects.filter(resource_name=course)

		if resources:
			context_dict['resources'] = resources
	except Course.DoesNotExist, Department.DoesNotExist:
		pass
	return render(request, template_name='noun/_resources_list.html', context=context_dict)


def user_login(request):
	context_dict = {}
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')

		user = authenticate(username=username, password=password)
		if user:
			if user.is_active:
				login(request, user)
				return HttpResponseRedirect('/noun/')
			else:
				return HttpResponse("Your account has been disabled")
		else:
			print "invalid login details : {0}, {1}".format(username, password)
			return HttpResponse("Invalid login details supplied")
	else:
		return render(request, template_name='noun/_login_page.html', context=context_dict)


@login_required
def logout(request):
	logout(request)
	return HttpResponseRedirect('/noun/register')
