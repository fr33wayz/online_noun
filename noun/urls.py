__author__ = 'peter'

from django.conf.urls import url, patterns, include
from  noun import views

urlpatterns = patterns(
	'',
	url(r'^$', view=views.index, name="index"),
	url(r'^register/', view=views.registration, name='registration'),
	url(r'^schools/', view=views.schools_page, name='schools'),
	url(r'^department/(?P<school_slug>[\w\-]+)/$', view=views.get_schools_department, name='department'),
	url(r'^course/(?P<dept_id>\d+)/$', view=views.get_department_course, name='course'),
	url(r'^resource/(?P<course_slug>[\w\-]+)/$', view=views.resources_list, name="resource"),
	url(r'^login/', view=views.user_login, name="login"),
	url(r'^logout/', view=views.logout, name='logout')

)
