from django.contrib import admin
from noun.models import School, Department, UserProfile, Resource, Course
# Register your models here.


admin.site.register(School)
admin.site.register(Department)
admin.site.register(UserProfile)
admin.site.register(Resource)
admin.site.register(Course)