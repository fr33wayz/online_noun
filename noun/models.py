from django.contrib.auth.models import User
from django.db import models
from django.template.defaultfilters import slugify


# Create your models here.


class School(models.Model):
	school_name = models.CharField(max_length=255)
	school_description = models.CharField(max_length=255)
	slug = models.SlugField(unique=True, null=True)

	def save(self, *args, **kwargs):
		self.slug = slugify(self.school_name)
		super(School, self).save(*args, **kwargs)

	def __unicode__(self):
		return self.school_name


class Department(models.Model):
	department_name = models.CharField(max_length=225)
	department_description = models.CharField(max_length=225)
	dept_faculty = models.ForeignKey(School)

	def __unicode__(self):
		return self.department_name


class Course(models.Model):
	course_code = models.CharField(max_length=225)
	course_name = models.CharField(max_length=225)
	course_dept = models.ForeignKey(Department)
	slug = models.SlugField(unique=True, null=True)

	def save(self, *args, **kwargs):
		self.slug = slugify(self.course_name)
		super(Course, self).save(*args, **kwargs)

	def __unicode__(self):
		return str(self.course_name)


class Resource(models.Model):
	resource_name = models.ForeignKey(Course)
	uploaded_date = models.DateField(auto_now_add=True)
	document_url = models.FileField(upload_to='resources', blank=True)

	def __unicode__(self):
		return self.resource_name.course_name


class UserProfile(models.Model):
	user = models.OneToOneField(User)
	student_id = models.CharField(max_length=255)
	department = models.ForeignKey(Department)

	def __unicode__(self):
		return self.user.username
